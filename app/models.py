from app import db, login
from mongoengine import Document, StringField, ReferenceField, DateTimeField
import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

@login.user_loader
def load_user(id):
    return User.objects(id = id)

class User(UserMixin, Document):
    username = StringField(required = True)
    email = StringField(requried = True)
    passwordHash = StringField()

    def __repr__(self):
        return f'<User {self.username}>'

    def setPassword(self, password):
        self.passwordHash = generate_password_hash(password)
    
    def checkPassword(self, password):
        return check_password_hash(self.passwordHash, password)

class Post(Document):
    body = StringField(required = True)
    timestamp = DateTimeField(required = True, default = datetime.datetime.now)
    author = ReferenceField(User, required = True)